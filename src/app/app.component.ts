import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  numbers: Array<number> = new Array<number>();

  onIntervalFired(n: number) {
    console.log(`interval fired: ${n}`);
    this.numbers.push(n);
  }
}
