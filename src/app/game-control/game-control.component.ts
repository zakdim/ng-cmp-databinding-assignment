import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {

  @Output('intervalFired') intervalFired = new EventEmitter<number>();

  intervalRef = null;
  lastNumber = 0;

  constructor() { }

  ngOnInit(): void {
  }

  onStartGame() {
    console.log('onStartGame');
    this.intervalRef = setInterval(() => {
      // console.log(`count = ${this.count}`);
      this.intervalFired.emit(this.lastNumber++);
    }, 1000);
  }

  onPauseGame() {
    if (this.intervalRef) {
      console.log('onPauseGame');
      clearInterval(this.intervalRef);
    }
  }
}
